import os, random, requests

from flask import Flask, request, jsonify

app = Flask(__name__)
x = []

registered_hosts = set()

@app.route('/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route('/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def proxy(path):
	try:
		if path in '/reg':
			host = request.args.get('host', 'http://localhost')

			registered_hosts.add(host)
			return jsonify(list(registered_hosts))
		elif path in '/x':
			return jsonify(x)

		host = random.choice(list(registered_hosts))
		full_path = host + "/" + path

		if request.method == 'GET':
			r = requests.get(full_path)
		elif request.method == 'POST':
			r = requests.post(full_path, json=request.get_json())
		elif request.method == 'PUT':
			r = requests.put(full_path, json=request.get_json())
		elif request.method == 'DELETE':
			r = requests.delete(full_path)
		
		x.append({
			"host": host,
			"method": request.method,
			"path": path,
			"full_path": full_path,
			"req": request.get_json(),
			"r": r.content
		})
		return r.content, r.status_code
	except Exception as e:
		print e
		return jsonify({"error": str(e)}), 500


if __name__ == "__main__":
	app.run(host='0.0.0.0', port=os.environ['PORT'] or 80, threaded=False)
	