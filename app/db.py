import json, uuid, os, subprocess, time, re

import sync_interface

data_d = 'data'
cwd = os.getcwd()

def update_read_timestamp(timestamp, table=None, _id=None):
	if not table:
		if os.path.exists(os.path.join(cwd, data_d)):
			return [update_read_timestamp(timestamp, f) for f in os.listdir(os.path.join(cwd, data_d)) if os.path.isdir(os.path.join(cwd, data_d, f))]
	elif not _id:
		if os.path.exists(os.path.join(cwd, data_d, table)):
			return [update_read_timestamp(timestamp, table, f) for f in os.listdir(os.path.join(cwd, data_d, table)) if os.path.isfile(os.path.join(cwd, data_d, table, f)) and not re.search(r"\.timestamp\.", f)]
	else:
		if os.path.exists(os.path.join(cwd, data_d, table, _id)):
			with open(os.path.join(cwd, data_d, table, _id + ".timestamp.read"), 'w+') as f:
				f.write(str(timestamp))
	return None

def update_write_timestamp(timestamp, table, _id):
	if os.path.exists(os.path.join(cwd, data_d, table, _id)):
		with open(os.path.join(cwd, data_d, table, _id + ".timestamp.write"), 'w+') as f:
			f.write(str(timestamp))
	return None

def read_sync_times(table, _id):
	write_timestamp = None
	read_timestamp = None
	if os.path.exists(os.path.join(cwd, data_d, table, _id + ".timestamp.write")):
		with open(os.path.join(cwd, data_d, table, _id + ".timestamp.write"), 'r') as f:
			write_timestamp = float(f.read().strip())
	if os.path.exists(os.path.join(cwd, data_d, table, _id + ".timestamp.read")):
		with open(os.path.join(cwd, data_d, table, _id + ".timestamp.read"), 'r') as f:
			read_timestamp = float(f.read().strip())
	return write_timestamp, read_timestamp

def get_data(table=None, _id=None, request=None, rec=False):

	if not table:
		if os.path.exists(os.path.join(cwd, data_d)):
			x = {f: get_data(f, request=request, rec=True) for f in os.listdir(os.path.join(cwd, data_d)) if os.path.isdir(os.path.join(cwd, data_d, f))}
			if not rec:
				timestamp = time.time()
				update_read_timestamp(timestamp)
				sync_interface.read_time_sync(timestamp)
			return x
	elif not _id:
		if os.path.exists(os.path.join(cwd, data_d, table)):
			x = [get_data(table, f, request=request, rec=True) for f in os.listdir(os.path.join(cwd, data_d, table)) if os.path.isfile(os.path.join(cwd, data_d, table, f)) and not re.search(r"\.timestamp\.", f)]
			if not rec:
				timestamp = time.time()
				update_read_timestamp(timestamp, table)
				sync_interface.read_time_sync(timestamp, table)
			return x
	else:
		if os.path.exists(os.path.join(cwd, data_d, table, _id)):
			with open(os.path.join(cwd, data_d, table, _id)) as f:
				if request and not request.args.get('internal', False):
					new_read_timestamp = time.time()
					time.sleep(10)

					write_timestamp, read_timestamp = read_sync_times(table, _id)

					if new_read_timestamp > write_timestamp and new_read_timestamp > read_timestamp:
						if not rec:
							update_read_timestamp(new_read_timestamp, table, _id)
							sync_interface.read_time_sync(new_read_timestamp, table, _id)

					else:
						raise Exception('Data you are looking for was already modified, please issue a fresh request')
				read_data = json.load(f)
				return {'_id': _id, 'data': read_data}
	return None

def update_data(table, _id, request):
	updateData = request.get_json()

	internal = request.args.get('internal', False)

	if not _id:
		_id = str(uuid.uuid1())
	elif not os.path.exists(os.path.join(cwd, data_d, table, _id)) and not internal:
		return None

	if not os.path.exists(os.path.join(cwd, data_d)):
		os.makedirs(os.path.join(cwd, data_d))

	if not os.path.exists(os.path.join(cwd, data_d, table)):
		os.makedirs(os.path.join(cwd, data_d, table))
	
	new_write_timestamp = time.time()
	if not internal:

		if os.path.exists(os.path.join(cwd, data_d, table, _id)):
			write_timestamp, read_timestamp = read_sync_times(table, _id)

			if new_write_timestamp > write_timestamp and new_write_timestamp > read_timestamp:
				update_write_timestamp(new_write_timestamp, table, _id)
			else:
				raise Exception('Data you are looking for was already modified, please issue a fresh request')
		else:
			update_write_timestamp(new_write_timestamp, table, _id)
	else:
		update_write_timestamp(updateData['timestamp'], table, _id)
	
	with open(os.path.join(cwd, data_d, table, _id), 'w+') as f:
		json.dump(updateData, f)
		if not internal:
			updateData['timestamp'] = new_write_timestamp
			sync_interface.send('PUT', table, _id, updateData)
	
	return {
		'_id': _id,
		'data': updateData
	}


def delete_data(table, _id=None, request=None):
	internal = request.args.get('internal', False)
	if not table:
		if os.path.exists(os.path.join(cwd, data_d)):
			subprocess.call(["rm", "-rf", os.path.join(cwd, data_d, '*')])
			if not internal:
				sync_interface.send('DEL')
	elif not _id:
		if os.path.exists(os.path.join(cwd, data_d, table)):
			subprocess.call(["rm", "-rf", os.path.join(cwd, data_d, table)])
			if not internal:
				sync_interface.send('DEL', table)
	else:
		if os.path.exists(os.path.join(cwd, data_d, table, _id)):
			subprocess.call(["rm", "-rf", os.path.join(cwd, data_d, table, _id + "*")])
			if not internal:
				sync_interface.send('DEL', table, _id)
	return ''