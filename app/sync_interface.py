import requests, os

# import interface

def send(action, table=None, _id=None, data=None):
	try:
		if action == 'REG':
			r = requests.get(os.environ['SYNC_MASTER'] + "/reg?host=" + os.environ['HOST'])
			s = requests.get(os.environ['SERVER_MASTER'] + "/reg?host=" + os.environ['HOST'])
		else:
			data['_id'] = _id
			send_data = {
				'action': action,

				'table': table,
				'_id': _id,
				'data': data
			}
			requests.post(os.environ['SYNC_MASTER'] + "/sync?host=" + os.environ['HOST'], json=send_data)
	except Exception as e:
		print e

def read_time_sync(timestamp, table=None, _id=None):
	send_data = {
		"timestamp": timestamp,

		"table": table,
		"_id": _id
	}
	requests.post(os.environ['SYNC_MASTER'] + "/sync_read_time?host=" + os.environ['HOST'], json=send_data)