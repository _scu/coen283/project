import interface, sync_interface, os

if __name__ == "__main__":
	sync_interface.send('REG')
	# interface.socketio.run(interface.app, port=os.environ['PORT'])
	interface.app.run(host='0.0.0.0', port=os.environ['PORT'] or 80)