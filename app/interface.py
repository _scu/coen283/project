from flask import Flask, request, jsonify

import util, db

app = Flask(__name__)

@app.route('/')
def get():
	try:
		return jsonify(util.normalise(db.get_data(request=request))), 200
	except Exception as e:
		print e
		return jsonify({"error": str(e)}), 500
	

@app.route('/<table>', methods=['GET', 'POST', 'DELETE'])
def get_or_put_or_delete_by_table(table=None):
	try:
		if request.method == 'POST':
			return jsonify(util.normalise(db.update_data(table=table, _id=None, request=request))), 201
		elif request.method == 'DELETE':
			return db.delete_data(table=table, request=request), 204
		elif request.method == 'GET':
			if not table:
				return get()
			return jsonify(util.normalise(db.get_data(table=table, request=request))), 200
	except Exception as e:
		print e
		return jsonify({"error": str(e)}), 500

@app.route('/<table>/<_id>', methods=['GET', 'PUT', 'DELETE'])
def get_or_put_or_delete_by_id(table=None, _id=None):
	try:
		if request.method == 'PUT':
			return jsonify(util.normalise((db.update_data(table=table, _id=_id, request=request)))), 200
		elif request.method == 'DELETE':
			if not _id:
				return get_or_put_or_delete_by_table(table)
			return db.delete_data(table=table, _id=_id, request=request), 204
		elif request.method == 'GET':
			if not _id:
				return get_or_put_or_delete_by_table(table)
			return jsonify(util.normalise((db.get_data(table=table, _id=_id, request=request)))), 200
	except Exception as e:
		print e
		return jsonify({"error": str(e)}), 500

@app.route('/timestamp/<timestamp>')
@app.route('/timestamp/<timestamp>/<table>')
@app.route('/timestamp/<timestamp>/<table>/<_id>')
def sync_time(timestamp, table=None, _id=None):
	try:
		db.update_read_timestamp(timestamp, table, _id)
		return jsonify({}), 200
	except Exception as e:
		print e
		return jsonify({"error": str(e)}), 500
