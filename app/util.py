def normalise(data):
	if isinstance(data, list):
		for i in range(len(data)):
			data[i] = normalise(data[i])
	elif isinstance(data, dict):
		if "timestamp" in data:
			del data["timestamp"]
		if "_id" in data and "data" in data:
			data["data"] = normalise(data["data"])
			data["data"]["_id"] = data["_id"]
			return data["data"]
		else:
			for i in iter(data):
				data[i] = normalise(data[i])
	return data