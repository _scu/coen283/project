# COEN283 Final Project

With the ever-growing technological expansion, distributed systems have become the need of the hour. A distributed system is a network of computers working together as one single unit. The components of this system communicate with each other and coordinate their actions to achieve a common goal of this system. 

## Framework and tools
1. Python
2. Flask
3. Heroku
4. Shell

## Start up
1. To start the sync server
    ```shell
    export SYNC=True

    export PORT=8080 # defaults to 80 if not provided
    python setup.py

2. To start the database
    ```shell
    export HOST=https://mj-coen283-db1.herokuapp.com # defaults to http://localhost
    export SYNC_MASTER=https://mj-coen283-sync.herokuapp.com
    export SERVER_MASTER=https://mj-coen283-db.herokuapp.com # WIP (Can be ignored for now)

    export PORT=8080 # defaults to 80 if not provided
    python setup.py

3. (WIP) To start the loadbalance server
    ```shell
    export SERVER=True

    export PORT=8080 # defaults to 80 if not provided
    python setup.py


## Current Deployment

Type | Link | Status
---|---|---
Sync server | https://mj-coen283-sync.herokuapp.com | ![Sync](https://heroku-badge.herokuapp.com/?app=mj-coen283-sync)
Database 1 | https://mj-coen283-db1.herokuapp.com | ![DB1](https://heroku-badge.herokuapp.com/?app=mj-coen283-db1)
Database 2 | https://mj-coen283-db2.herokuapp.com | ![DB2](https://heroku-badge.herokuapp.com/?app=mj-coen283-db1)
*(WIP)* Load-balance server | https://mj-coen283-db.herokuapp.com | ![LB](https://heroku-badge.herokuapp.com/?app=mj-coen283-db)