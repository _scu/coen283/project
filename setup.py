import  os
if 'SERVER' in os.environ:
	import server.server
	server.server.app.run(host='0.0.0.0', port=os.environ['PORT'] or 80, processes=1, threaded=False)
elif 'SYNC' in os.environ:
	import sync.server
	sync.server.app.run(host='0.0.0.0', port=os.environ['PORT'] or 80)
else:
	import app.interface, app.sync_interface
	app.sync_interface.send('REG')
	app.interface.app.run(host='0.0.0.0', port=os.environ['PORT'] or 80)