import os, json, requests

from flask import Flask, request, jsonify

app = Flask(__name__)

registered_hosts = set()
x = []

def get_host(request):
	return request.args.get('host', 'http://localhost')


@app.route('/')
def get_register():
	return jsonify(list(registered_hosts))

@app.route('/x')
def get_x():
	return jsonify(x)

@app.route('/reg')
def register():
	host = get_host(request)
	
	registered_hosts.add(host)
	return jsonify(list(registered_hosts))

@app.route('/sync', methods=['POST'])
def sync():
	data_string = request.data
	data = json.loads(data_string)
	
	path = "/" + data['table']

	if data['_id']:
		path += "/" + data['_id']

	path += "?internal=True"

	this_host = get_host(request)
	if this_host not in registered_hosts:
		registered_hosts.add(this_host)

	for host in registered_hosts:
		if this_host in host:
			continue
		try:
			data_x = data
			data_x["hostfsd"] = host + path
			x.append(data_x)
			if data['action'] == 'PUT':
				requests.put(host + path, json=data["data"])
			elif data['action'] == 'DEL':
				requests.delete(host + path)
		except Exception as e:
			print e
			continue
	return jsonify({})

@app.route('/sync_read_time', methods=['POST'])
def sync_time():
	data_string = request.data
	data = json.loads(data_string)
	
	path = "/timestamp/" + str(data['timestamp'])

	if data['table']:
		path += "/" + data['table']

	if data['_id']:
		path += "/" + data['_id']

	path += "?internal=True"

	this_host = get_host(request)
	if this_host not in registered_hosts:
		registered_hosts.add(this_host)

	for host in registered_hosts:
		if this_host in host:
			continue
		try:
			data_x = data
			data_x["hostfsd"] = host + path
			x.append(data_x)
			requests.get(host + path)
		except Exception as e:
			print e
			continue
	return jsonify({})

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=os.environ['PORT'] or 80)
	